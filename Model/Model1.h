#ifndef MODEL1_H
#define MODEL1_H

#include "Model.h"

class Model1 : public Model
{
	private:
		Controller_base* ptr_c_;
		int x_;
	public:
		Model1(int x) : x_(x) { } //constructor assign the value to x_.
		virtual void set_controller(Controller_base* ptr_c); // set sthe controller to model1.
		void change(int x); // changes the value of the model object.
		int get_x(); // returns the value of the model object.
};

#endif