#include "Model1.h"

void Model1::set_controller(Controller_base* ptr_c)
{
    ptr_c_ = ptr_c;
}

void Model1::change(int x)
{
    x_ = x;
    ptr_c_->update();
}

int Model1::get_x()
{
    return x_;
}	