#ifndef MODEL_H
#define MODEL_H

#include "../Controller/Controller_base.h"

class Model 
{
	public:
		virtual ~Model() { } 
		virtual void set_controller(Controller_base* ptr_c) = 0; 
		virtual void change(int) = 0; 
		virtual int get_x() = 0; 
};

#endif