#ifndef CONTROLLER_H
#define CONTROLLER_H

// #include "Controller_base.h"
#include "../Model/Model.h"
#include "../View/View.h"

class Controller : public Controller_base
{
	private:
		Model* ptr_m_; //holds address of a model (data)
		View *ptr_v1_; //holds address of a view (display data)
	public:
		Controller(Model* ptr_m) : ptr_m_(ptr_m) { } // pass the address of the model and stores in ptr_m_
		virtual void add(View* ptr_v); // add -> sets the view address.
		virtual void update(); // updates -> updates the view.
};

#endif