#ifndef VIEW_SQ_H
#define VIEW_SQ_H

#include <iostream>
using namespace std;
#include "View.h"

class View_sq : public View
{
	private:
		Model *ptr_model_;
	public:
		virtual void update(); // gets the value of model and does manipulation with respect to it.
		virtual void get_model(Model *ptr_model); // assign the address of the model to the pointer.
		virtual void register_view(Controller_base* ptr_c); // assigns the address of view to controller.
};

#endif