//client program

#include <iostream>
using namespace std;

// includes the header modules
#include "Controller/Controller.h"
#include "Model/Model1.h"
#include "View/View_sq.h"


int main()
{
	Model1 m(10); // create a object for Model1 
	Controller c(&m); // pass the address of model to the controller
	m.set_controller(&c); // pass the address of controller to the model 
	
	View_sq view_sq; //
	view_sq.register_view(&c);
	view_sq.get_model(&m);
	
	m.change(20);
}